# impatient-org-common

Eventually intended to be a basic expense database.

## Class Suffixes

* `D` - maps directly to a database table; mutable.
* `E` - record from a database table, enhanced with information from other tables (deprecated)
* `I` - immutable info object (often a D object enhanced with data from other tables)



## Application Concepts

### Labels

Labels are used to group accounts, transfers, and transactions.
Labels can imply each other, creating a hierarchy.
For example, the label Groceries could imply the label Expenses,
meaning that every transfer labeled Groceries also implicitly has the label Expenses.
The *implies* relationship is sometimes called parent/child: the child implies the parent.

### Foods

A Nutrient is any statistic we want to record for a piece of food.
The flags tell whether it is measured by mass, energy, etc.

A Food is a definition of a food, with nutrition facts.
It may be divided into portions, and portionDesc describes how the portions are defined (e.g. maybe "per slice" for a pizza).
If nutrition facts are given for the whole food (box), nutrientPortions is 1.
The Food definition exists to give you ways to quickly reuse nutrition facts you already entered when you buy the same food again.

A StoredFood is a piece of food currently stored in the fridge, etc.
It references a Food definition, and any changes to the definition will affect the StoredFood.

A FoodConsumption is a time when you ate a particular piece of food.
The (interpolated) nutrients you consumed are associated with the consumption event,
and will not change if the definition of the Food later changes.

For a food, the fractionRemaining and fractionConsumed are normally positive and no greater than 1.
But greater numbers may be allowed, e.g. if you buy/eat 2 packages of the same food.