package imp.org.load

import imp.org.LabelRelation
import imp.org.OrgLabel
import imp.org.OrgModel
import imp.org.orgOrm
import imp.sqlite.Database
import imp.util.logger
import kotlin.reflect.KClass

/**Generic utility for associating labels with something else.
 * You provide the model class representing "something else", and the table that relates labels to something else,
 * and the column name in that table that refers to something else (e.g. somethingId).
 * Then this class can find labels by something, or update the labels something has.*/
class LabelAssociator <M: OrgModel, R : LabelRelation> (
    modelClass: KClass<M>, //not used, but specifying this will help the caller notice if they're passing the wrong type of object
    private val relClass: KClass<R>,
    /**The table column that doesn't refer to a label. For instance, if we're associating people to labels, this would be something like personId.*/
    private val otherCol: String
) {
    private val log = LabelAssociator::class.logger
    private val relTable = orgOrm.table(relClass)

    fun get(model: M, db: Database): List<OrgLabel> = get(model.id)

    fun get(modelId: Long, db: Database): List<OrgLabel> {
        if(model.id == -1L)
            return emptyList()
        val records = orgOrm.query(relTable).andWhere("$otherCol = ?").param(model.id).selectAll(db)
        return records.map {LabelLoader.require(it.labelId)}
    }


    /**Sets which labels apply to a model.*/
    fun update(modelId: Long, desiredLabels: List<OrgLabel>, db: Database) {
        update(modelId, get(modelId, db), desiredLabels, db)
    }

    /**Sets which labels apply to a model, given the set of labels that currently apply.*/
    fun update(modelId: Long, currentLabels: List<OrgLabel>, desiredLabels: List<OrgLabel>, db: Database) {
        val add = HashSet(desiredLabels).also {it.removeAll(currentLabels)}
        val remove = HashSet(currentLabels).also {it.removeAll(desiredLabels)}

        db.beginTransaction()
        log.debug("Adding {} and removing {} labels for {} {}.", add.size, remove.size, otherCol, modelId)

        if(add.isNotEmpty()) {
            db.prepareInsert("INSERT INTO ${relClass.simpleName} ($otherCol, labelId) VALUES (?, ?)").use { stmt ->
                stmt.setLong(1, modelId)
                for(label in add) {
                    stmt.setLong(2, label.data.id)
                    stmt.insert()
                }
            }
        }

        if(remove.isNotEmpty()) {
            db.prepareUpdate("DELETE FROM Account_Label WHERE accountId = ? AND labelId = ?").use {stmt ->
                stmt.setLong(1, modelId)
                for(label in remove) {
                    stmt.setLong(2, label.id)
                    stmt.execute()
                }
            }
        }

        db.commit()
    }


    /**Removes all labels from the model. Make sure you call this before deleting a model.*/
    fun removeAll(modelId: Long, db: Database) {
        db.prepareUpdate("DELETE FROM ${relClass.simpleName} WHERE $otherCol = ?").use {stmt ->
            stmt.setLong(1, modelId)
            stmt.update()
        }
    }
}