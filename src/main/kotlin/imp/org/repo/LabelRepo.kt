package imp.org.repo

import imp.org.LabelD
import imp.org.Label_Label
import imp.org.OrgLabel
import imp.org.orgOrm
import imp.orm.kt.Query
import imp.orm.kt.Table
import imp.sqlite.Database
import imp.util.logger

private val log = LabelRepo::class.logger


object LabelRepo {
	private lateinit var table: Table<LabelD>
	private lateinit var relation: Table<Label_Label>
	private var initialized = false
	private val byId = HashMap<Long, OrgLabel>()


	private fun initIfNecessary() {
		if(initialized)
			return
		log.debug("Loading all labels.")
		table = orgOrm.table(LabelD::class)
		relation = orgOrm.table(Label_Label::class)

		TODO()
//		OrgDb.connect().use { db ->
//			orgOrm.query(table).selectEach(db) { record ->
//				byId[record.id] = OrgLabel(record)
//			}
//
//			Query(relation).selectEach(db) { record ->
//				val parent = byId[record.parentId] ?: throw Exception("Nonexistent parent ID: ${record.parentId}")
//				val child = byId[record.childId] ?: throw Exception("Nonexistent child ID: ${record.childId}")
//				parent.children.add(child.data)
//				child.parents.add(parent.data)
//			}
//		}

		initialized = true
		log.debug("Loaded {} labels.", byId.size)
	}


	@Synchronized fun require(id: Long): OrgLabel {
		initIfNecessary()
		return byId[id]?.deepCopy() ?: throw Exception("Label not found: $id")
	}

	@Synchronized fun save(label: OrgLabel, db: Database) {
		initIfNecessary()
		val compare = if(label.data.id == -1L) OrgLabel.newUnsaved() else byId[label.data.id] ?: throw Exception("Label not found: ${label.data.id}")
		val create = label.data.id == -1L
		TODO()
//		db.withTransaction {
//			log.debug("Saving label {} {}.", label.data.id, label.data.name)
//			table.save(label.data, db)
//			if(create)
//				log.debug("New label has ID {}.", label.data.id)
//
//			//children
//			val children = DiffUtil.diff(compare.children.map {it.id}, label.children.map {it.id})
//			log.debug("Adding {} children, removing {}.", children.added.size, children.removed.size)
//			for(child in children.added)
//				addRelation(label.data.id, child, db)
//			for(child in children.removed)
//				deleteRelation(label.data.id, child, db)
//
//			//parents
//			val parents = DiffUtil.diff(compare.parents.map {it.id}, label.parents.map {it.id})
//			log.debug("Adding {} parents, removing {}.", parents.added.size, parents.removed.size)
//			for(parent in parents.added)
//				addRelation(parent, label.data.id, db)
//			for(parent in parents.removed)
//				deleteRelation(parent, label.data.id, db)
//		}

		byId[label.data.id] = label.deepCopy()
	}


	private fun addRelation(parent: Long, child: Long, db: Database) {
		relation.save(Label_Label(parentId = parent, childId = child), db)
	}

	private fun deleteRelation(parent: Long, child: Long, db: Database) {
		Query(relation).andWhere("parentId = ? AND childId = ?").param(parent).param(child).delete(db)
	}


	@Synchronized fun delete(label: OrgLabel, db: Database) {
		log.debug("Deleting {}.", label)
		if(label.data.id == -1L)
			throw IllegalArgumentException("Label was never created.")
		TODO()
//		db.withTransaction {
//			table.deletePk(label.data.id, db)
//			Query(relation).andWhere("parentId = ? OR childId = ?").param(label.data.id).param(label.data.id).delete(db)
//		}
		byId.remove(label.data.id)
	}


	@Synchronized fun getAll(): ArrayList<OrgLabel> {
		initIfNecessary()
		val out = ArrayList<OrgLabel>(byId.size)
		for(label in byId.values)
			out.add(label.deepCopy())
		return out
	}
}


