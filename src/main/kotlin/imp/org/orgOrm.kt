package imp.org

import imp.orm.kt.Orm


val orgOrm = Orm(listOf(
	LabelD::class,
	// finance
	UnitD::class,
	AccountD::class,
	AccountBalanceD::class,
	TransferD::class,
	TransactD::class,
	Label_Label::class,
	Account_Label::class,
	Transact_Label::class,
	Transfer_Label::class,

	// food
	NutrientD::class,
	FoodD::class,
	FoodNutrientD::class,
	StoredFoodD::class,
	FoodConsumptionD::class,
	FoodConsumptionNutrientD::class,
))