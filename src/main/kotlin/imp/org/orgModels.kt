package imp.org

import java.util.*

//this file contains all the basic concepts in this app that are stored, but as high-level views rather than the literal database representation

/**Interface for all high-level data objects, which wrap data objects with extra info.*/
interface OrgModel {
	val data: OrgData
//	var id: Long
//	var uuid: UUID
}


class OrgLabel (
	override val data: LabelD,
//	override var id: Long,
//	override var uuid: UUID,
//	var name: String,
//	var description: String,
) : OrgModel  {
	val children = HashSet<LabelD>()
	val parents = HashSet<LabelD>()

	override fun toString() = "Label(${data.id} ${data.name})"

	companion object {
		fun newUnsaved() = OrgLabel(LabelD(-1, UUID.randomUUID(), "", ""))
	}

	fun deepCopy() = OrgLabel(data.copy()).also {
		for(child in children)
			it.children.add(child.copy())
		for(parent in parents)
			it.parents.add(parent.copy())
	}
}

///**A unit like grams or dollars.*/
//class OrgUnit (
//	override var id: Long,
//	override var uuid: UUID,
//	var name: String,
//	var description: String,
//	var fractionalDigits: Int,
//) : OrgModel {
//	override fun toString() = "Unit($id $uuid $name)"
//}
//
//
//class Account (
//	override var id: Long,
//	override var uuid: UUID,
//	var name: String,
//	var description: String,
//	var unit: OrgUnit,
//	val labels : List<OrgLabel>,
//) : OrgModel {
//	override fun toString() = "Account($id $uuid $name)"
//}
//
//class AccountBalance (
//	override var id: Long,
//	override var uuid: UUID,
//	var account: Account,
//	var amount: UnitAmount,
//	var timeMs: Long,
//	var description: String,
//) : OrgModel {
//	override fun toString() = "AccountBalance($id $uuid ${amount.amount} for ${account.id} at $timeMs)"
//}
//
//class Transfer (
//	override var id: Long,
//	override var uuid: UUID,
//	var from: Account?,
//	var to: Account?,
//	var amount: UnitAmount,
//	var timeMs: Long,
//	var title: String,
//	var description: String,
//	val labels: List<OrgLabel>,
//	var transaction: OrgTransaction? = null,
//) : OrgModel {
//	override fun toString() = "Transfer($id $uuid from ${from?.id} to ${to?.id} ${amount.amount} at $timeMs)"
//}
//
///**For possible future use - currently not used.*/
//class OrgTransaction (
//	override var id: Long,
//	override var uuid: UUID,
//	var timeMs: Long,
//	var transfers: List<Transfer>
//) : OrgModel {
//	override fun toString() = "Transaction($id $uuid ${transfers.size} transfers @ $timeMs)"
//}