package imp.org

import imp.orm.kt.*
import java.util.*

//This file contains low-level data objects that are converted directly to/from database records.
//Most class names end with "D", which stands for "data".
//The special "relation" classes use a different naming convention.



/**Interface for most database tables, so we can access the ID and UUID generically.*/
interface OrgData {
	var id: Long
	var uuid: UUID
}


@OrmTable("Label") data class LabelD (
	@PrimaryKey override var id: Long,
	override var uuid: UUID,
	var name: String,
	var description: String?,
) : OrgData

@OrmTable("Unit") data class UnitD (
	@PrimaryKey override var id: Long,
	override var uuid: UUID,
	var name: String,
	var short: String?,
	var symbol: String?,
	var description: String?,
	var fractionalDigits: Int,
) : OrgData

@OrmTable("Account") data class AccountD (
	@PrimaryKey override var id: Long,
	override var uuid: UUID,
	var name: String,
	var description: String?,
	@References("Unit") var unitId: Long
) : OrgData

@OrmIndex("AccountBalance_recent", "accountId, timeMs DESC")
@OrmTable("AccountBalance") data class AccountBalanceD (
	@PrimaryKey override var id: Long,
	override var uuid: UUID,
	@References("Account") var accountId: Long,
	var amount: Long,
	var timeMs: Long,
	var title: String?,
	var description: String?,
) : OrgData

@OrmTable("Transfer") data class TransferD (
	@PrimaryKey override var id: Long,
	override var uuid: UUID,
	@References("Account") var fromAccountId: Long?,
	@References("Account") var toAccountId: Long?,
	var amount: Long,
	var title: String?,
	var description: String?,
	@References("Transaction") var transactionId: Long?,
) : OrgData

@OrmTable("Transaction") data class TransactD (
	@PrimaryKey override var id: Long,
	override var uuid: UUID,
	var timeMs: Long,
	var title: String?,
	var description: String?,
) : OrgData



//relations

/**Interface for a table that relates labels to some other table, so we can access the label ID generically.*/
interface LabelRelation {
	var labelId: Long
}


class Label_Label (
	@PrimaryKey var id: Long = -1,
	@References("Label") var parentId: Long,
	@References("Label") var childId: Long,
)

class Account_Label (
	@PrimaryKey var id: Long,
	@References("Account") var accountId: Long,
	@References("Label") override var labelId: Long
) : LabelRelation

class Transfer_Label (
	@PrimaryKey var id: Long,
	@References("Transfer") var transferId: Long,
	@References("Label") override var labelId: Long
) : LabelRelation

class Transact_Label (
	@PrimaryKey var id: Long,
	@References("Transaction") var transactId: Long,
	@References("Label") override var labelId: Long
) : LabelRelation