package imp.org

import imp.orm.kt.OrmIndex
import imp.orm.kt.OrmTable
import imp.orm.kt.PrimaryKey
import imp.orm.kt.References
import java.util.*


@OrmIndex("Nutrient_uuid", "uuid", unique = true)
@OrmTable("Nutrient") data class NutrientD (
	@PrimaryKey var id: Long,
	var uuid: UUID,
	var flags: Int,
	var name: String,
	var desc: String?,
	var sortOrder: Int,
) {
	object Flags {
		/**when displaying, just display the unit, not the name.*/
		val showOnlyUnit = 1
		/**The nutrient measures energy (calories, etc.)*/
		val isEnergy = 1 shl 1
		/**The nutrient measures mass (lbs, kg, etc.)*/
		val isMass = 1 shl 2
	}
}

@OrmIndex("Food_uuid", "uuid", unique = true)
@OrmTable("Food") data class FoodD (
	@PrimaryKey var id: Long,
	var uuid: UUID,
	var name: String,
	var desc: String?,
	var nutrientPortions: Double,
	var portionDesc: String?,
	var updatedMs: Long,
	/**The last time we know the nutrition facts were correct (no earlier than updatedMs).*/
	var verifiedMs: Long,
)
@OrmTable("FoodNutrient") data class FoodNutrientD (
	@PrimaryKey var id: Long,
	@References("Food") var foodId: Long,
	@References("Nutrient") var nutrientId: Long,
	var amount: Double,
)

@OrmTable("StoredFood") data class StoredFoodD (
	@PrimaryKey var id: Long,
	var uuid: UUID,
	@References("Food") var foodId: Long,
	var acquiredMs: Long,
	var expiresMs: Long?,
	var fractionRemaining: Double,
)

@OrmTable("FoodConsumption") data class FoodConsumptionD (
	@PrimaryKey var id: Long,
	var uuid: UUID,
	var ms: Long,
	@References("Food") var foodId: Long?,
	/**The fraction of the food that was consumed, normally 0-1 but can be greater.*/
	var fractionConsumed: Double,
)
@OrmTable("FoodConsumptionNutrient") data class FoodConsumptionNutrientD (
	@PrimaryKey var id: Long,
	@References("StoredFood") var storedFoodId: Long?,
	@References("Nutrient") var nutrientId: Long,
	/**How much of the nutrient was consumed.*/
	var amount: Double,
)