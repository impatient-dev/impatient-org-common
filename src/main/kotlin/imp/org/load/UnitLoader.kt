package imp.org.load

import imp.org.LabelD
import imp.org.OrgUnit
import imp.org.UnitD
import java.util.*

object UnitLoader : SimpleFullyCachedLoader<OrgUnit, UnitD>("Unit", UnitD::class) {
	override fun dataFactory(model: OrgUnit) = UnitD(model.id, model.uuid.toString(), model.name, model.description, model.fractionalDigits)
	override fun modelFactory(data: UnitD, uuid: UUID) = OrgUnit(data.id, uuid, data.name, data.description, data.fractionalDigits)
}