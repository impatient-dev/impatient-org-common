package imp.org.load

import imp.org.AccountBalance
import imp.org.AccountBalanceD
import imp.org.orgOrm
import imp.sqlite.Database
import imp.util.logger

object BalanceLoader {
    private val log = this::class.logger
    private val table = orgOrm.table(AccountBalanceD::class)

    fun save(model: AccountBalance, db: Database) {
        log.debug("Saving {}", model)
        val data = AccountBalanceD(model.id, model.uuid.toString(), model.account.id, model.amount.amount, model.timeMs, model.description)
        table.save(data, db)
        model.id = data.id //if this was an insert, the ID changed
    }

    fun delete(model: AccountBalance, db: Database) {
        log.debug("Deleting {}.", model)
        table.deletePk(model.id, db)
        model.id = -1
    }
}