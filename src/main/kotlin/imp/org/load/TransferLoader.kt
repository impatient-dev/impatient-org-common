package imp.org.load

import imp.org.Transfer
import imp.org.TransferD
import imp.org.Transfer_Label
import imp.org.orgOrm
import imp.sqlite.Database
import imp.util.logger

object TransferLoader {
    private val log = this::class.logger
    private val table = orgOrm.table(TransferD::class)

    val labels = LabelAssociator(Transfer::class, Transfer_Label::class, "transferId")

    fun save(model: Transfer, db: Database) {
        log.debug("Saving {}", model)
        db.beginTransaction()
        val data = TransferD(model.id, model.uuid.toString(), model.from?.id, model.to?.id, model.amount.amount, model.title, model.description, model.transaction?.id)
        table.save(data, db)
        labels.update(data.id, model.labels, db)
        db.commit()
        model.id = data.id //if this was an insert, the ID changed
    }

    fun delete(model: Transfer, db: Database) {
        db.beginTransaction()
        log.debug("Deleting {}.", model)
        labels.removeAll(model.id, db)
        table.deletePk(model.id, db)
        db.commit()
        model.id = -1
    }
}