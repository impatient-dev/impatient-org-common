package imp.org.load

import imp.org.Account
import imp.org.AccountD
import imp.org.Account_Label
import imp.org.orgOrm
import imp.sqlite.Database
import imp.util.logger

object AccountLoader {
    private val log = this::class.logger
    private val table = orgOrm.table(AccountD::class)
    private val labelTable = orgOrm.table(Account_Label::class)

    val labels = LabelAssociator(Account::class, Account_Label::class, "accountId")

    fun save(model: Account, db: Database) {
        log.debug("Saving {}", model)
        db.beginTransaction()
        val data = AccountD(model.id, model.uuid.toString(), model.name, model.description, model.unit.id)
        table.save(data, db)
        labels.update(data.id, model.labels, db)
        db.commit()
        model.id = data.id //if this was an insert, the ID changed
    }

    fun delete(model: Account, db: Database) {
        db.beginTransaction()
        log.debug("Deleting {}.", model)
        labels.removeAll(model.id, db)
        table.deletePk(model.id, db)
        db.commit()
        model.id = -1
    }
}