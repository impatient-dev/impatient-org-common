package imp.org.load

import imp.org.OrgData
import imp.org.OrgDb
import imp.org.OrgModel
import imp.org.orgOrm
import imp.sqlite.Database
import imp.util.logger
import java.util.*
import java.util.concurrent.ConcurrentHashMap
import kotlin.reflect.KClass

/**Basic loader class that loads all values from a table initially, then uses that cache forever. Thread-safe and concurrent.
 * To avoid breaking the cache, never modify any model this loader returns, or any model you have provided to this loader.*/
abstract class SimpleFullyCachedLoader <M: OrgModel, D: OrgData> (
		/**What the model (and data) represents - something like "Person".*/
		private val name: String,
		dataClass: KClass<D>
) {
	private val log = this::class.logger
	private val table = orgOrm.table(dataClass)
	private val actualIdMap = ConcurrentHashMap<Long, M>()
	private val actualUuidMap = ConcurrentHashMap<UUID, M>()
	@Volatile private var initialized = false

	/**Returns the map, but first loads everything if we haven't yet.*/
	private val uuidMap: ConcurrentHashMap<UUID, M> get() {
		if(initialized)
			return actualUuidMap
		synchronized(this) {
			if(!initialized)
				OrgDb.connect().use {db ->
					init(db)
				}
			return actualUuidMap
		}
	}

	/**Returns the map, but first loads everything if we haven't yet.*/
	private val idMap: ConcurrentHashMap<Long, M> get() {
		if(initialized)
			return actualIdMap
		synchronized(this) {
			if(!initialized)
				OrgDb.connect().use {db ->
					init(db)
				}
			return actualIdMap
		}
	}


	fun save(model: M, db: Database) {
		uuidMap.compute(model.uuid) { _, _ -> //get a lock so no one else modifies this at the same time
			log.debug("Saving {}.", model)
			val data = dataFactory(model)
			table.save(data, db)
			model.id = data.id //if this was an insert, the ID changed
			model
		}
		actualIdMap[model.id] = model
	}

	fun delete(model: M, db: Database) {
		log.debug("Deleting {}.", model)
		table.deletePk(model.id, db)
		idMap.remove(model.id)
		model.id = -1
		uuidMap.remove(model.uuid)
	}

	fun require(id: Long) : M = idMap[id] ?: throw Exception("$name not found: $id")
	fun require(uuid: UUID) : M = uuidMap[uuid] ?: throw Exception("$name not found: $uuid")

	fun getAll(): List<M> = ArrayList(uuidMap.values)

	private fun init(db: Database) {
		log.debug("Initializing.")
		orgOrm.query(table).selectEach(db) {
			val uuid = UUID.fromString(it.uuid)
			actualUuidMap[uuid] = modelFactory(it, uuid)
		}
		initialized = true
		log.debug("Loaded ${actualUuidMap.size} of $name from table ${table.name}.")
	}


	protected abstract fun dataFactory(model: M): D
	protected abstract fun modelFactory(data: D, uuid: UUID): M
}